const deleteCredEvent = new Event('deletePass');
const showAlertEvent = new Event('showalert');

const showAlert = (type, title, message) => {
    return `
        <div class="alert alert-${type} alert-dismissible fade show" role="alert">
                <strong>${title}</strong> ${message}
                <button type="button" class="btn-close" data-bs-dismiss="alert"
                aria-label="Close"></button>
        </div>
    `
}

const handleSignupSubmit = (event) => {
    event.preventDefault();
    const formData = new FormData(event.target)
    const username = formData.get("username");
    const password = formData.get("password");
    if (password === formData.get("confirmPassword")) {
        eel.sign_up(username, password)((res) => {
            if (res) {
                alert("Signup Successful ! Now Login")
                window.location.replace("index.html")
            }
            else {
                alert("Oops!!")
            }
        })
    }
    else {
        document.querySelector("#result").innerHTML = "<small class='lead'>Passwords do not match</small>"
    }
}

const handleLoginSubmit = (event) => {
    event.preventDefault();
    const formData = new FormData(event.target)
    const username = formData.get("username");
    eel.authenticate(username, formData.get("password"))((res) => {
        if (res) {
            localStorage.setItem("token", res)
            localStorage.setItem("username", username)
            window.location.replace("dashboard.html")
        }
        else {
            document.querySelector("#result").innerHTML = showAlert("danger", "Oops!", "Error While Logging in. Please check your inputs.")
        }
    })

}

const logout = () => {
    eel.logout(localStorage.username, localStorage.token)((res) => {
        if (res) {
            localStorage.clear()
            window.location.replace("index.html")
        }
    })
}

const handleAddCredentialSubmit = (event) => {
    event.preventDefault()
    const formData = new FormData(event.target);
    const application = formData.get("application");
    const username = formData.get("username");
    const password = formData.get("password");
    const notes = formData.get("notes")

    if (password === formData.get("confirmPassword")) {
        eel.addCredential(application, username, password, notes, localStorage.getItem("token"))((res) => {
            if (res) {
                document.querySelector("#addCredentialForm").reset();
                document.dispatchEvent(showAlertEvent);
                document.querySelector("#result").innerHTML = showAlert('success',
                    'Credential Added!',
                    'You can see this credential in <strong data-bs-dismiss="alert" class="text-primary" onclick="document.querySelector(`#home-tab`).click()" style="cursor:pointer;">Home</strong> tab.')
            }
        })
    }
    else {
        document.querySelector("#result").innerHTML = showAlert("danger", "Try Again!", "Passwords do not match.")
    }
}

const updatePassword = (credential_id, application, username, password, notes) => {
    document.querySelector("#credential_id").value = credential_id
    document.querySelector("#application").value = application;
    document.querySelector("#username").value = username;
    document.querySelector("#password").value = password;
    document.querySelector("#updatedNotes").value = notes;
}

const handleUpdateSubmit = (event) => {
    event.preventDefault();
    const formData = new FormData(event.target);
    const credential_id = formData.get("credential_id")
    const application = formData.get("application");
    const username = formData.get("username");
    const password = formData.get("password");
    const notes = formData.get("notes")

    eel.updateCredential(credential_id, application, username, password, notes, localStorage.token)((res) => {
        if (res) {
            document.querySelector("#modalClose").click()
            getCredentials()
            document.querySelector("#listResult").innerHTML = showAlert("success", "Done!", `Details of ${res} updated successfully.`)
        }
        else {
            alert("Whoopsy!!")
        }
    })

}

const deletePassword = (credential_id) => {
    eel.deleteCredential(credential_id, localStorage.token)(res => {
        if (res) {
            getCredentials();
            document.dispatchEvent(deleteCredEvent);
            document.querySelector("#listResult").innerHTML = showAlert('success', 'Done!', 'Credential removed successfully.')
        }
        else {
            document.querySelector("#listResult").innerHTML = showAlert('warning', 'Oops!', 'Error occured while deleting credential.')
        }
    })
}
const copyPass = (id) => {
    let passText = document.getElementById(id)
    passText.select();
    passText.setSelectionRange(0, 99999);
    navigator.clipboard.writeText(passText.value);

    const toast = new bootstrap.Toast(document.getElementById('liveToast'))
    toast.show()
}

const getCredentials = () => {

    eel.showCredentials(localStorage.username, localStorage.token)((res) => {
        if (res) {

            renderTable(res);
        }
        else {
            alert("whoopsy")
        }
    })
}

const handleSearchInput = (event) => {
    console.log(event.target.value);
    // def search(query:str, token:str, username:str):
    eel.search(event.target.value, localStorage.token, localStorage.username)((res) => {
        (res) ? renderTable(res) : alert("Oops !!")
    })
}

function renderTable(res) {
    let table = "";

    res = JSON.parse(res);
    res.forEach((row) => {
        const credential_id = row.credential_id;
        table += `
                <tr>
                <td>${row.application}</td>
                <td>${row.username}</td>
                <td class="text-center"><i class="bi bi-clipboard2" onclick=copyPass("Cred${credential_id}") style="cursor:pointer;"></i><input id="Cred${credential_id}" value='${row.password}' style="display:None;"></td>
                <td>${row.notes}</td>
                <td> <span class="icon" data-bs-toggle="modal" onclick="updatePassword('${credential_id}','${row.application}','${row.username}','${row.password}','${row.notes}');" data-bs-target="#updateCredentialModal"><i class="bi bi-pencil-fill icon"></i> Edit</span> / <span class="icon" onclick="deletePassword(${credential_id})"><i class="bi bi-trash-fill icon"></i>Delete</span></td>
                </tr>
                `;
    });

    document.querySelector("#credsBody").innerHTML = table;
    return res;
}

document.addEventListener('deletePass', function () {
    setTimeout(function () {
        document.querySelector("#listResult").innerHTML = "";
    }, 3000);
});
document.addEventListener('showalert', () => {
    setTimeout(function () {
        document.querySelector("#result").innerHTML = "";
    }, 3000);
})