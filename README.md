# Password Manager
A password manager built using Python and Vanilla JS !

### Installation
Run following commands:
```
git clone https://github.com/adityasovani/Password-Manager.git
cd password-manager
pip install -r requirements.txt
```
EXE can be found in ```output/my_pass.zip```