from traceback import print_exc
import eel
from bcrypt import checkpw, hashpw, gensalt
from sqlite3 import connect
from uuid import uuid4
from hashlib import md5
from cryptocode import encrypt,decrypt
from pandas import DataFrame

key = None
userID = None

def checkToken(token:str, username:str):
    conn = connect("app.db")
    try:
        user = conn.execute(f"SELECT token from users WHERE username = '{username}'")
        result = user.fetchall()[0]
        return token == result[0]
    except:
        print_exc()

@eel.expose()
def authenticate(username: str, password: str) -> bool:
    conn = connect("app.db")
    try:
        user = conn.execute(f"SELECT username,password from users WHERE username = '{username}'")
        if user:
            user = user.fetchall()[0]
            if checkpw(password=password.encode(), hashed_password=user[1].encode()):
                token = uuid4().hex.upper()
                # Set token in DB
                conn.execute(f"UPDATE users SET token = '{token}' WHERE username = '{username}'")
                global userID, key
                userID = username
                key = md5(password.encode()).hexdigest()

                conn.commit()
                conn.close()
                return token
            else:
                print("NOT SAME")
                return False
    except:
        print_exc()
        return False

@eel.expose()
def logout(username:str, token:str) -> bool:
    if checkToken(token=token, username=username):
        try:
            conn = connect("app.db")
            conn.execute(f"UPDATE users SET token = NULL WHERE username = '{username}'")
            conn.commit()
            conn.close()
            return True
        except:
            print_exc()
    return False

@eel.expose()
def showCredentials(username:str, token:str):
    if checkToken(token=token, username=username):
        conn = connect("app.db")
        try:
            creds = conn.execute(f"SELECT credential_id,application,username, password, notes FROM credentials WHERE userId = '{username}'")
            creds = creds.fetchall()
            
            credentials = DataFrame({"credential_id":[],"application":[],"username":[], "password":[], "notes":[]})
            for i in creds:
                credentials.loc[len(credentials)] = i
            global key
            credentials['password'] = credentials["password"].apply(lambda x: decrypt(x, key))

            return credentials.to_json(orient="records")
        except:
            print_exc()
    return False

@eel.expose()
def addCredential(application:str, username:str, password:str, notes:str, token:str):
    global userID, key
    if checkToken(token=token, username=userID):
        conn = connect("app.db")
        password = encrypt(password, key)
        try:
            conn.execute("INSERT into credentials(application,username,password,notes,userId) VALUES(?,?,?,?,?)",(application,username, password,notes,userID))
            conn.commit()
            conn.close()
            return True
        except:
            print_exc()
            return False
    else:
        return None

@eel.expose()
def updateCredential(credential_id:int, application:str, username:str, password:str, notes:str, token:str):
    global userID, key
    if checkToken(token=token, username=userID):
        conn = connect("app.db")
        password = encrypt(password, key)
        try:
            conn.execute(f"UPDATE credentials SET application='{application}', username='{username}', password='{password}',notes='{notes}' WHERE credential_id={credential_id}")
            conn.commit()
            conn.close()
            return application
        except:
            print_exc()
            return False
    else:
        return None

@eel.expose()
def deleteCredential(credential_id,token:str):
    global userID
    if checkToken(token=token, username=userID):
        conn = connect("app.db")
        try:
            conn.execute(f"DELETE FROM credentials WHERE credential_id = {credential_id}")
            conn.commit()
            conn.close()
            return True
        except:
            print_exc()
    return False
    
@eel.expose()
def sign_up(username: str, password: str):
    try:
        password = hashpw(password=password.encode(), salt=gensalt()).decode()
        conn = connect("app.db")
        conn.execute("INSERT INTO users(username, password) VALUES(?,?)", (username, password))
        conn.commit()
        conn.close()
    except:
        return False
    return username

@eel.expose()
def search(query:str, token:str, username:str):
    global userID
    if checkToken(token=token, username=userID):
        conn = connect("app.db")
        query = f"SELECT credential_id, application, username, password, notes FROM credentials WHERE userId = '{username}' AND (application LIKE '%{query}%' OR username LIKE '%{query}%' OR notes LIKE '%{query}%')"
        result = conn.execute(query).fetchall()
        df = DataFrame(result, columns=['credential_id', 'application', 'username', 'password', 'notes'])
        conn.close()
        return df.to_json(orient='records')
    return False
    

if __name__ == "__main__":
    eel.init("Web")
    eel.start("index.html")
